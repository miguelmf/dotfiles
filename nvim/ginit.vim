" Enable Mouse
set mouse=a

" Set Editor Font
if exists(':GuiFont')
    " Use GuiFont! to ignore font errors
    " GuiFont! Fira Code:h14
    "GuiFont! JetBrains Mono:h15
    GuiFont! JetBrains Mono Light:h15
    " GuiFont! Iosevka:h14.5
endif
GuiPopupmenu 0
GuiRenderLigatures 1
" GuiTreeviewToggle 1
"GuiTreeviewShow
